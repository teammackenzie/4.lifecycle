import React, { Component, Fragment } from 'react';
import './Header.css';
import Alert from './Alert';
export default class Header extends Component {
	constructor() {
		console.log(`Header: constructor()`);
		super();
		this.state = {
			msgBienvenido: 'Hola App!',
			mensaje: true
		};
	}
	componentDidMount() {
		console.log(`Header: componentDidMount()`);
	}
	componentDidUpdate(prevProps, prevState) {
		console.log(prevState);
		console.log(`Header: componentDidUpdate()`);
	}

	handleChangeMsg = () => this.setState({ mensaje: !this.state.mensaje });
	render() {
		console.log(`Header: render()`);
		const { tituloApp, year } = this.props;
		return (
			<Fragment>
				<div className="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded shadow-sm">
					<img
						className="mr-3"
						src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQlvW34rPLX9uAHIcnkFHj845UiE5Uam4i0sWetpa1nRMrsZ3glsQ"
						alt=""
						width="48"
						height="48"
					/>
					<div className="lh-100">
						<h6 className="mb-0 text-white lh-100">{tituloApp}</h6>
						<small>Since {year}</small>
					</div>
				</div>
				{this.state.mensaje && <Alert msgBienvenido={this.state.msgBienvenido} />}

				<button onClick={this.handleChangeMsg} type="button" className="btn btn-primary">
					{this.state.mensaje ? 'Ocultar' : 'Mostrar'}
				</button>
			</Fragment>
		);
	}
}
