import React, { Component } from 'react';

export default class Alert extends Component {
	componentWillUnmount() {
		console.log(`Alert: componentWillUnmount()`);
	}
	render() {
		const { msgBienvenido } = this.props;
		return (
			<div className="alert alert-primary" role="alert">
				{msgBienvenido}
			</div>
		);
	}
}
