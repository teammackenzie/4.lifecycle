import React, { Component } from 'react';
import Header from './Header';

export default class Main extends Component {
	state = {
		tituloApp: 'App Gastos',
		year: new Date().getFullYear(),
		gastos: []
	};
	handlerNewGasto = gasto => {
		this.setState({ gastos: [...this.state.gastos, gasto] });
	};

	componentDidMount() {
		console.log(`Main: componentDidMount()`);
	}
	render() {
		console.log(`Main: render()`);
		return (
			<div className="container">
				<Header tituloApp={this.state.tituloApp} year={this.state.year} />
			</div>
		);
	}
}
